<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('departments','departmentsController');
Route::resource('programs','programsController');
Route::resource('semesters','semestersController');
Route::resource('subjects','subjectsController');
Route::get('/mysubjects','subjectsController@mysubjects')->name('subjects.mysubjects');
Route::resource('papers','papersController');
Route::get('/mypapers','usersController@mypapers')->name('users.mypapers');

Route::resource('questions','questionsController');
Route::get('/teachers', 'usersController@teachers')->name('users.teachers');
Route::get('/hod', 'usersController@hod')->name('users.hod');
Route::get('/dashboard', 'dashboardController@index')->name('dashboard.index');
