<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\department;
use App\User;
use App\Http\Requests\departments\insert;
use App\Http\Requests\departments\update;
class departmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware(['auth','can:admin']);
    }
    public function index()
    {
        return view('departments.index')->with('departments',department::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.create')
        ->with('teachers',User::where('role','teacher')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(insert $request)
    {
     department::create([
        'department_name' => $request->department_name,
        'hod' => $request->department_hod
     ]);   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(department $department)
    {
        return view('departments.create')
        ->with('department',$department)
        ->with('teachers',User::where('role','teacher')->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(update $request,department $department)
    {
        if($department->hod == NULL && $request->department_hod == NULL){
            //do nothing
        }
        else if($department->hod == NULL && $request->department_hod != NULL){
            //change that teacher tole to hod
        }
        else if($department->hod != NULL && $request->department_hod == NULL){
            // change that hod role to teacher
        }
        else{
            // first make current hod role to teacher
            // then make selected teacher role to hod
        }     

        $department->update([
            'department_name' => $request->department_name,
            'hod' => $request->department_hod
         ]);  
         session()->flash('success','Department Updated Successfully');
         return redirect(route('departments.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
