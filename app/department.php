<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class department extends Model
{
    protected $fillable = ['department_name','hod'];

    public function head(){
        return $this->belongsTo(User::class,'hod');
    }
}
