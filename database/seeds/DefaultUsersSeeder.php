<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
class DefaultUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teacher = User::where('email','bilalteacher@gmail.com')->first();
        if(!$teacher){
             User::create([
                'name' => "M. Bilal Naeem",
                'email' => "bilalteacher@gmail.com",
                'cnic' => '33102-1396937-7',
                'password' => Hash::make('shahjy')
            ]);          
        }
        $hod = User::where('email','bilalhod@gmail.com')->first();
        if(!$hod){
            User::create([
                'name' => "M. Bilal Naeem",
                'email' => "bilalhod@gmail.com",
                'cnic' => '33102-1396457-7',
                'role' => 'hod',
                'password' => Hash::make('shahjy')
            ]);          
        }
        $admin = User::where('email','bilaladmin@gmail.com')->first();
        if(!$admin){
            User::create([
                'name' => "M. Bilal Naeem",
                'email' => "bilaladmin@gmail.com",
                'cnic' => '33102-1392537-7',
                'role' => 'admin',
                'password' => Hash::make('shahjy')
            ]);          
        }
    }
}
