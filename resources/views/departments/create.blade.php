@extends('adminlte::page')

@section('content')
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                @if(isset($department))
                  Edit Department
                @else
                  Add Department
                @endif
                </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="POST" action="{{ isset($department) ? route('departments.update',$department->id) : route('departments.store') }}">
                  @csrf
                  @if(isset($department))
                    @method('PATCH')
                  @endif
                <div class="card-body">
                  <div class="form-group">
                    <label for="department_name">Department Name</label>
                    <input type="text" class="{{ $errors->has('department_name') ? 'is-invalid':'' }} form-control"
                     name="department_name" value="{{ isset($department) ? $department->department_name : '' }}" id="department_name" placeholder="Enter Department Name">
                    @if($errors->has('department_name'))
                    <span class="error invalid-feedback">{{ $errors->first('department_name') }}</span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="departmentHOD">HOD</label><br />
                    <select class="select2" name="department_hod" id="">
                        <option value="">Leave it as for now</option>
                      @if(isset($department) && isset($department->head))
                      <option value="{{ $department->head->id }}" selected="selected" >{{ $department->head->name }}</option>
                      @endif
                    @forelse($teachers as $teacher)
                        <option value="{{ $teacher['id'] }}">{{ $teacher['name'] }}</option>    
                    @empty
                        <p>No Values to show</p>
                    @endforelse
                    </select>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">
                  @if(isset($department))
                    Update
                  @else
                    Add
                  @endif
                  </button>
                </div>
              </form>
            </div>
@endsection

@section('js')
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  })
  </script>
@endsection