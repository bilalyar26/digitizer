<?php

namespace App\Http\Requests\departments;

use Illuminate\Foundation\Http\FormRequest;

class update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'department_name' => 'required|max:255|unique:departments,department_name,'.$this->department->id
        ];
    }
}
